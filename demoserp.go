package main

import (
	"bufio"
	"context"
	"encoding/json"
	"log"
	"math/rand"
	"net/url"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/chromedp/cdproto/cdp"
	"github.com/chromedp/chromedp"
)

type Result struct {
	Link string `json:"link"`
}

const (
	port             = 9515
	sleepMin         = 11
	sleepMax         = 45
	numPerRequest    = 100
	outputFilePrompt = "Enter output file path: "
	queryPrompt      = "Enter query: "
)

var (
	opts []chromedp.ExecAllocatorOption
	r    = rand.New(rand.NewSource(time.Now().UnixNano()))
)

func main() {
    setupOptions()

    baseContext := context.Background()
    ctx, cancel := chromedp.NewExecAllocator(baseContext, opts...)
    defer cancel()

    outputFile := getInput(outputFilePrompt)
    query := getInput(queryPrompt)

    totalResults, resultStats, err := getSearchResults(ctx, query)
    if err != nil {
        log.Fatal(err)
    }
	
	time.Sleep(100 * time.Millisecond)

	parts := strings.Split(resultStats, " ")
	totalResults, _ = strconv.Atoi(parts[1])


    numRequests := totalResults / numPerRequest
    if totalResults%numPerRequest > 0 {
        numRequests++
    }

    var allResults []Result

ua := generateUserAgent()
opts = append(opts, chromedp.UserAgent(ua))

for i := 0; i < numRequests; i++ {
    start := i * numPerRequest

    resultsThisPage := getResultsOnPage(ctx, query, start)

    for _, res := range resultsThisPage {
        allResults = append(allResults, Result{Link: res.Link})
    }
}

// Context cancellation after the loop
cancel() // Cancel the context created with NewExecAllocator


    ua = generateUserAgent()
    opts = append(opts, chromedp.UserAgent(ua))

    for i := 0; i < numRequests; i++ {
        start := i * numPerRequest

        resultsThisPage := getResultsOnPage(ctx, query, start)

        for _, res := range resultsThisPage {
            allResults = append(allResults, Result{Link: res.Link})
        }
    }

    saveResultsToFile(allResults, outputFile)
}

func setupOptions() {
	opts = []chromedp.ExecAllocatorOption{
		chromedp.Flag("headless", true),
		chromedp.Flag("window-size", "1536,864"),
		chromedp.Flag("window-position", "0,0"),
		chromedp.Flag("disable-infobars", true),
		chromedp.Flag("disable-extensions", true),
		chromedp.Flag("hide-scrollbars", true),
		chromedp.Flag("mute-audio", true),
		chromedp.Flag("disable-notifications", true),
		chromedp.Flag("disable-gpu", true),
		chromedp.Flag("no-sandbox", true),
		chromedp.Flag("disable-dev-shm-usage", true),
		chromedp.Flag("disable-features", "site-per-process"),
		chromedp.Flag("disable-background-networking", true),
		chromedp.Flag("disable-sync", true),
		chromedp.Flag("disable-background-timer-throttling", true),
		chromedp.Flag("disable-backgrounding-occluded-windows", true),
		chromedp.Flag("disable-ipc-flooding-protection", true),
		chromedp.Flag("disable-breakpad", true),
		chromedp.Flag("disable-client-side-phishing-detection", true),
		chromedp.Flag("disable-component-extensions-with-background-pages", true),
		chromedp.Flag("disable-default-apps", true),
		chromedp.Flag("disable-features", "TranslateUI,BlinkGenPropertyTrees"),
		chromedp.Flag("disable-hang-monitor", true),
		chromedp.Flag("disable-ipc-flooding-protection", true),
		chromedp.Flag("disable-popup-blocking", true),
		chromedp.Flag("disable-prompt-on-repost", true),
		chromedp.Flag("disable-renderer-backgrounding", true),
		chromedp.Flag("disable-sync-preferences", true),
		chromedp.Flag("disable-web-security", true),
		chromedp.Flag("enable-automation", true),
		chromedp.Flag("force-color-profile", "srgb"),
		chromedp.Flag("metrics-recording-only", true),
		chromedp.Flag("password-store", "basic"),
		chromedp.Flag("use-mock-keychain", true),
		chromedp.Flag("remote-debugging-port", strconv.Itoa(port)),
		chromedp.Flag("disable-logging", true),
		chromedp.Flag("no-first-run", true),
		chromedp.Flag("disable-background-timer-throttling", true),
		chromedp.Flag("disable-backgrounding-occluded-windows", true),
		chromedp.Flag("disable-databases", true),
		chromedp.Flag("disable-extensions", true),
		chromedp.Flag("disable-geolocation", true),
		chromedp.Flag("disable-infobars", true),
		chromedp.Flag("disable-media-source", true),
		chromedp.Flag("disable-notifications", true),
		chromedp.Flag("disable-offer-store-unmasked-wallet-cards", true),
		chromedp.Flag("disable-popup-blocking", true),
		chromedp.Flag("disable-print-preview", true),
		chromedp.Flag("disable-prompt-on-repost", true),
		chromedp.Flag("disable-renderer-backgrounding", true),
		chromedp.Flag("disable-sync", true),
		chromedp.Flag("disable-translate", true),
		chromedp.Flag("disable-web-security", true),
		chromedp.Flag("disable-session-crashed-bubble", true),
		chromedp.Flag("disable-tab-for-desktop-share", true),
		chromedp.Flag("enable-automation", true),
		chromedp.Flag("enable-blink-features", "IdleDetection"),
		chromedp.Flag("force-color-profile", "srgb"),
		chromedp.Flag("no-default-browser-check", true),
		chromedp.Flag("no-experiments", true),
		chromedp.Flag("password-store", "basic"),
		chromedp.Flag("use-mock-keychain", true),
		chromedp.Flag("use-mobile-user-agent", true),
		chromedp.Flag("remote-debugging-port", strconv.Itoa(port)),
	}
}

func getInput(prompt string) string {
	log.Print(prompt)
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	return scanner.Text()
}

func getSearchResults(ctx context.Context, query string) (int, string, error) {
	var totalResults int
	var resultStats string

	err := chromedp.Run(ctx,
		chromedp.Navigate("https://www.google.com/search?q="+url.QueryEscape(query)+"&num="+strconv.Itoa(numPerRequest)),
		chromedp.Text(`#result-stats`, &resultStats),
	)
	if err != nil {
		return 0, "", err
	}

	parts := strings.Split(resultStats, " ")
	totalResults, _ = strconv.Atoi(parts[1])
	return totalResults, resultStats, nil
}

func generateUserAgent() string {
	uas := []string{
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
        "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/72.0.3626.121 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.124 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/91.0.4472.164 Safari/537.36",
		"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/92.0.4515.107 Safari/537.36",
	}

	return uas[r.Intn(len(uas))]
}

func getResultsOnPage(ctx context.Context, query string, start int) []Result {
	var resultsThisPage []Result
	var nodes []*cdp.Node

	// Create a new context for each iteration
	newCtx, cancel := context.WithCancel(ctx)
	defer cancel()

	err := chromedp.Run(newCtx,
		chromedp.Navigate(buildGoogleURL(query, start)),
		chromedp.WaitReady("div#search"),
		chromedp.Nodes("div#search a", &nodes, chromedp.ByQueryAll),
	)
	if err != nil {
		log.Fatal(err)
	}

	for _, node := range nodes {
		resultsThisPage = append(resultsThisPage, Result{Link: node.NodeValue})
	}

	return resultsThisPage
}


func buildGoogleURL(query string, start int) string {
	return "https://www.google.com/search?q=" + url.QueryEscape(query) +
		"&num=" + strconv.Itoa(numPerRequest) +
		"&start=" + strconv.Itoa(start)
}

func saveResultsToFile(results []Result, outputFile string) {
	output, err := json.MarshalIndent(results, "", "  ")
	if err != nil {
		log.Fatal(err)
	}

	err = os.WriteFile(outputFile, output, 0644)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Results saved to %s", outputFile)
}
